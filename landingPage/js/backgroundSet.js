$('.bodyIndex').hide();
$("header").css('opacity', '0.0');

// $(document).ready(function () {
//     $(".loader").hide()
//     setTimeout(function () { $("#LogoW").fadeIn(100) }, 50)
// });

$(window).load(function () {
    $("header").hide()
    $("#loading").fadeOut(300);

    setTimeout(function () {
        $("header").css('opacity', '1.0');
        $('.bodyIndex').fadeIn(1000);
        $('header').fadeIn(1000);

        // //SET BACKGOUND SIZE
        // var backgroundHeight = $(".row").outerHeight() + $(".twoCol").outerHeight() + $(".portfolioIndex").outerHeight() + $("#footer").outerHeight() + 50
        // $(".grayColBackground").css({ "height": backgroundHeight + "px" });
        // $(window).on("resize", function () {
        //     $(".grayColBackground").css({
        //         "height": backgroundHeight + "px"
        //     })
        // });

    }, 350)
});