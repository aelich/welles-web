$ aws s3 sync . s3://welles.sg --exclude ".git/*"

$ aws cloudfront list-distributions
$ aws cloudfront create-invalidation --distribution-id E2MIKOIG7HU7M0 --paths '/*'
$ aws cloudfront list-invalidations --distribution-id E194650BP30F3A

With the latest aws-cli python command line tools, to recursively delete all the files under a folder in a bucket is just:
$ aws s3 rm --recursive s3://your_bucket_name/foo/

Or delete everything under the bucket:
$ aws s3 rm --recursive s3://your_bucket_name

If what you want is to actually delete the bucket, there is one-step shortcut:
$ aws s3 rb --force s3://your_bucket_name